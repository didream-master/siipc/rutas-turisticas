import { NodeId, Neighbor, Node } from "../interfaces";
const DEFAULT_RLC_LENGTH = 4;
const DEFAULT_ITERATIONS = 10;

export interface BaseOptions {
    origin?: NodeId;
    rlcLength?: number;
    iterations?: number;
}
export abstract class BaseAlgorithm {
    public readonly nodes: Node[];
    public readonly pathLength: number;
    public origin: NodeId;
    protected rlcLength: number;
    protected iterations: number;

    constructor(nodes: Node[], options: BaseOptions = {}) {
        this.nodes = nodes;
        this.pathLength = this.nodes.length;
        this.setOptions(options);
    }

    public abstract run(options?: BaseOptions): any;

    public setOptions(options: BaseOptions = {}) {
        const {
            origin = this.origin || 0,
            rlcLength = this.rlcLength || DEFAULT_RLC_LENGTH,
            iterations = this.iterations || DEFAULT_ITERATIONS
        } = options;
        
        this.origin = origin;
        this.rlcLength = rlcLength;
        this.iterations = iterations;
    }

    public greedyRandomizedConstruction(): NodeId[] {
        const solution: NodeId[] = [];
        
        solution.push(this.origin);

        while(solution.length < this.pathLength) {
            let loc: NodeId = this.selectRLCMember(this.buildRCL(solution));
            solution.push(loc);
        }

        return solution;
    }

    // Restrictive Candidate List
    public buildRCL(excludedNodes: NodeId[]): NodeId[]{
        const freeNodes: Set<NodeId> = this.getFreeNodes(excludedNodes);
        const lastNodeId: NodeId = excludedNodes[excludedNodes.length - 1];
        return this.nodes[lastNodeId].neighbors
            .filter((neighbor: Neighbor) => freeNodes.has(neighbor.nodeId))
            .slice(0, this.rlcLength)
            .map((neighbor: Neighbor) => neighbor.nodeId);
    }

    public selectRLCMember(nodes: NodeId[]): NodeId {
        return nodes[Math.floor(Math.random() * nodes.length)];
    }

    protected randomNode(): NodeId {
        return Math.floor(Math.random() * this.nodes.length);
    }

    protected getFreeNodes(excludedNodes: NodeId[]): Set<NodeId> {
        return this.nodes.reduce((freeNodes: Set<NodeId>, node: Node): Set<NodeId>  => {
            excludedNodes.every((n: NodeId) => n != node.nodeId) && freeNodes.add(node.nodeId);
            return freeNodes;
        }, new Set());
    }

    protected calcCost(proposedNodes: NodeId[]): number {
        let cost = 0;
        for (let i=0; i < proposedNodes.length - 1; i++) {
            const proposedNode: Node = this.nodes[proposedNodes[i]];
            const iNextNode: NodeId = proposedNodes[i + 1];
            cost += proposedNode.costs[iNextNode];
        }
        const lastNode = this.nodes[proposedNodes[proposedNodes.length -1]];
        const firstNodeId = proposedNodes[0];
        cost += lastNode.costs[firstNodeId];
        return cost;
    }
}