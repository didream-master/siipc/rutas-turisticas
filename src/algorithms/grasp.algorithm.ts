import { NodeId } from '../interfaces';
import { SwappableAlgorithm } from './swappable.algorithm';
import { BaseOptions } from './base.algorithm';

export interface GRASPOptions extends BaseOptions {}

export class GRASP extends SwappableAlgorithm {
    
    public run(options?: GRASPOptions) {
        options && this.setOptions(options);
        let optimalSolution: any[] = [];
        let optimalSolutionCost = Number.MAX_VALUE;

        for(let i = 0; i < this.iterations; i++) {
            let candidateSolution: NodeId[] = this.greedyRandomizedConstruction();
            candidateSolution = this.localSearch(candidateSolution);
            const candidateSolutionCost: number = this.calcCost(candidateSolution);
            if (candidateSolutionCost < optimalSolutionCost) {
                optimalSolution = candidateSolution;
                optimalSolutionCost = candidateSolutionCost;
            }
        }

        return this.calcCost(optimalSolution);
    }

    protected localSearch(currentSolution: NodeId[]): NodeId[] {
        currentSolution = [...currentSolution];
        let solutionCost = this.calcCost(currentSolution);

        do {
            for (let i = 0; i < currentSolution.length - 1; i++ ) {
                const iAltNode = this.getBestNodeSwap(currentSolution, i);
                this.swap(currentSolution, i+1, iAltNode);
                solutionCost = this.calcCost(currentSolution);
            }
        } while(this.calcCost(currentSolution) < solutionCost);

        return currentSolution;
    }
}