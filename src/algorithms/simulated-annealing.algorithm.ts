import { Node, NodeId } from '../interfaces';
import { BaseAlgorithm, BaseOptions } from './base.algorithm';
const DEFAULT_TEMPERATURE = 20;
const DEFAULT_COLING_RATE = 0.05;

export interface SimulatedAnnealingOptions extends BaseOptions {
    temperature?: number;
    coolingRate?: number;
}
export class SimulatedAnnealing extends BaseAlgorithm {
    public temperature: number;
    public coolingRate: number;

    constructor(nodes: Node[], options : SimulatedAnnealingOptions = {}){
        super(nodes, options);
    }

    public setOptions(options: SimulatedAnnealingOptions = {}) {
        super.setOptions(options);
        const {temperature = this.temperature || DEFAULT_TEMPERATURE, coolingRate = this.coolingRate || DEFAULT_COLING_RATE} = options;
        this.temperature = temperature;
        this.coolingRate = coolingRate;
    }

    public run(options?: SimulatedAnnealingOptions): number {
        options && this.setOptions(options);
        let optimalSolution: NodeId[] = this.greedyRandomizedConstruction();
        let optimalSolutionCost: number = this.calcCost(optimalSolution);
        for (let temp = this.temperature; temp > 0.01; temp = this.cool(temp)) {
            for (let i=0; i < this.iterations; i++) {
                const candidateSolution = this.greedyRandomizedConstruction();
                const candidateSolutionCost = this.calcCost(candidateSolution);
                if (candidateSolutionCost < optimalSolutionCost ||
                    (Math.random() <  ( - Math.exp(candidateSolutionCost - optimalSolutionCost)) / temp )
                ) {
                    optimalSolution = candidateSolution;
                    optimalSolutionCost = candidateSolutionCost;
                }
            }
        }

        return optimalSolutionCost;
    }

    private cool(temp: number): number {
        return temp * (1 - this.coolingRate);
    }
}