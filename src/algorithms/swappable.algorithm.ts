import { BaseAlgorithm } from "./base.algorithm";
import { NodeId, Neighbor } from "../interfaces";

export abstract class SwappableAlgorithm extends BaseAlgorithm {
    protected getBestNodeSwap(currentSolution: NodeId[], iSolution: number): number {
        const nodeId = currentSolution[iSolution];
        const node = this.nodes[nodeId];
        const iNext = iSolution + 1;
        const neighborId = currentSolution[iNext];

        let bestINeighbor = iNext;
        let bestAltCost = this.calcCost(currentSolution);
        node.neighbors.forEach((altNeighbor: Neighbor) => {
            if (altNeighbor.nodeId != neighborId && altNeighbor.nodeId != this.origin) {
                const altSolution = [...currentSolution];
                const altISolution = currentSolution.findIndex(c => c == altNeighbor.nodeId);
                if (altISolution == -1) {
                    throw new Error(`altISolution: ${altISolution} es -1`);
                }
                this.swap(altSolution, iNext, altISolution);
                const altCost = this.calcCost(altSolution);
                if ( altCost < bestAltCost) {
                    bestAltCost = altCost;
                    bestINeighbor = altISolution;
                }
            }
        });
        return bestINeighbor;
    }

    protected swap(solution: NodeId[], iNodeA: number, iNodeB: number): void {
        const tmp = solution[iNodeA];
        solution[iNodeA] = solution[iNodeB];
        solution[iNodeB] = tmp;
    }
}