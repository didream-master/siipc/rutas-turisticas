
import { Neighbor, NodeId } from '../interfaces';
import { SwappableAlgorithm } from './swappable.algorithm';
import { BaseOptions } from './base.algorithm';

interface Swap {
    iA: number;
    iB: number;
    nodeIdA: NodeId;
    nodeIdB: NodeId;
    diff: number;
    alternativeCost: number
};
export interface TabuSearchOptions extends BaseOptions {}

export class TabuSearchAlgorithm extends SwappableAlgorithm {

    private bestSolutionCost: number;
    private tabu: {[key: number]: {[key: number]: number }} = {};
    private frecuencies: {[key: number]: {[key: number]: number }}= {};

    public run(options?: TabuSearchOptions): number {
        options && this.setOptions(options);
        let solution: NodeId[] = this.greedyRandomizedConstruction();
        this.bestSolutionCost = this.calcCost(solution);

        for (let i=0; i < this.iterations; i++) {
            this.refreshTabu();
            const bestSwap: Swap = this.getBestSwap(solution);
            this.addTabu(bestSwap);
            this.addFrecuencie(bestSwap);
            this.swap(solution, bestSwap.iA, bestSwap.iB);

            const currentCostSolution = this.calcCost(solution);
            if (currentCostSolution < this.bestSolutionCost) {
                this.bestSolutionCost = currentCostSolution;
            }
        }
        return this.calcCost(solution);
    }

    private getBestSwap(currentSolution: NodeId[]): Swap {
        const swaps: Swap[] = [];
        const currentCost = this.calcCost(currentSolution);
        let alternativeSolution;
        for (let iA=0; iA < currentSolution.length - 1; iA++) {
            alternativeSolution = [...currentSolution];
            const iCurrentNode: NodeId = currentSolution[iA];
            const bestNeighbor: Neighbor = this.nodes[iCurrentNode].neighbors[0];

            if (bestNeighbor == null) {
                throw new Error(`bestNeighbor nulo. CurrentNode: ${iCurrentNode}`);
            }

            const iB = this.getBestNodeSwap(currentSolution, iA);
            this.swap(alternativeSolution, iA, iB);

            const alternativeCost = this.calcCost(alternativeSolution);
            let diff: number = alternativeCost - currentCost;

            diff += this.getFrecuency(currentSolution[iA], currentSolution[iB]);

            if (!Number.isInteger(diff)) {
                throw new Error(`diff no es un número: diff: ${diff}, alternativeCost: ${alternativeCost}, currentCost: ${currentCost}`);
            }
            swaps.push({iA, iB, nodeIdA: currentSolution[iA], nodeIdB: currentSolution[iB], diff, alternativeCost});
        }

        
        swaps.sort((swapA , swapB): number => swapA.diff - swapB.diff);
        const bestSwap = swaps.find(swap => !this.isTabu(swap) || this.isPromising(swap));

        if (!bestSwap) {
            throw new Error('bestSwap inválido');
        }
        return bestSwap;
    }

    private getFrecuency(nodeIdA: NodeId, nodeIdB?: NodeId): number {
        return (nodeIdA in this.frecuencies && nodeIdB in this.frecuencies[nodeIdA] && this.frecuencies[nodeIdA][nodeIdB]) ||
                (nodeIdB in this.frecuencies && nodeIdA in this.frecuencies[nodeIdB] && this.frecuencies[nodeIdB][nodeIdA]) ||
                0;
    }

    private addFrecuencie(swap: Swap): void {
        const { nodeIdA, nodeIdB } = swap;
        if (nodeIdA in this.frecuencies && nodeIdB in this.frecuencies[nodeIdA]) {
            this.frecuencies[nodeIdA][nodeIdB] += 1;
        }
        else if (nodeIdB in this.frecuencies && nodeIdA in this.frecuencies[nodeIdB]) {
            this.frecuencies[nodeIdB][nodeIdA] += 1;
        }
        else this.frecuencies[nodeIdA] = Object.assign(this.frecuencies[nodeIdA] || {}, {[nodeIdB]: 1});
    }

    private addTabu(bestSwap: Swap): void {
        const { nodeIdA, nodeIdB } = bestSwap;
        if (nodeIdB in this.tabu && nodeIdA in this.tabu[nodeIdB]) {
            delete this.tabu[nodeIdB][nodeIdA];
        }
        this.tabu[nodeIdA] = Object.assign(this.tabu[nodeIdA] || {}, {[nodeIdB]: 10});
    }

    private isTabu(swap: Swap): boolean {
        const { nodeIdA, nodeIdB } = swap;
        return (this.tabu[nodeIdA] && (nodeIdB in this.tabu[nodeIdA]) ||
                this.tabu[nodeIdB] && (nodeIdA in this.tabu[nodeIdB]))
    }

    private refreshTabu() {
        Object.keys(this.tabu).forEach((i: any) => {
            Object.keys(this.tabu[i]).forEach((j: any) => {
                this.tabu[i][j]--;
                if (this.tabu[i][j] == 0) {
                    delete this.tabu[i][j];
                }
            })
        })
    }

    private isPromising(swap: Swap): boolean {
        return swap.alternativeCost < this.bestSolutionCost;
    }
}