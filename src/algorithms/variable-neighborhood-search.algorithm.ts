import { GRASP } from "./grasp.algorithm";
import { NodeId, Node } from "../interfaces";
import { BaseOptions } from "./base.algorithm";
const DEFAULT_K_MAX = 3;

export interface VariableNeighborhoodSearchOptions extends BaseOptions {
    kMax?: number
}
export class VariableNeighborhoodSearch extends GRASP {
    private kMax: number;
    constructor(nodes: Node[], options: VariableNeighborhoodSearchOptions = {}) {
        super(nodes, options);
    }

    public setOptions(options: VariableNeighborhoodSearchOptions = {}) {
        super.setOptions(options);
        const { kMax = this.kMax || DEFAULT_K_MAX } = options;
        this.kMax = kMax;
    }

    public run(options?: VariableNeighborhoodSearchOptions): any {
        options && this.setOptions(options);
        let solution: NodeId[] = this.greedyRandomizedConstruction();
        let solutionCost = this.calcCost(solution);
        for (let i=0; i < this.iterations; i++) {
            let k = 0;
            while (k < this.kMax) {
                let altSolution = this.shake(k);
                altSolution = this.localSearch(altSolution);
                const altCost = this.calcCost(altSolution);

                if (altCost < solutionCost) {
                    solutionCost = altCost;
                    solution = altSolution;
                    k = 0;
                }
                else {
                    k++;
                }
            }
        }
        return solutionCost;
    }


    private shake(k: number): NodeId[] {
        let shakedSolution = this.greedyRandomizedConstruction();
        for (let i=0; i<k; i++) {
            shakedSolution = this.greedyRandomizedConstruction();
        }
        return shakedSolution;
    }
}