import express from 'express';
import cors from 'cors';
import Database, { IMeasureResult, MeasureResult } from './config/initializers/database';
import main from './main';
import config from './config';

Database.connect();
const app = express();

app.use(cors());

app.get('/', async (req, res) => {
    return res.json( { ok: true } );
    /*
    try {
        res.json( { ok: true } );
        await main( async (resultExecution: IMeasureResult) => {
            let measureResult: IMeasureResult = await MeasureResult.findOne({
                algorithm: resultExecution.algorithm,
                testParams: resultExecution.testParams
            })

            if (measureResult) {
                const executions = measureResult.executions;
                const { cost: averageCost, timeExecution: averageTime } = measureResult.averages;
                measureResult.averages = {
                    cost: (executions * averageCost + resultExecution.averages.cost) / (executions + 1 ),
                    timeExecution: (executions * averageTime + resultExecution.averages.timeExecution) / (executions + 1)
                }
                measureResult.executions = executions + 1;
            }
            else {
                resultExecution.executions = 1;
                measureResult = new MeasureResult(resultExecution);
            }

            try {
                const savedInstance = await measureResult.save();
            }
            catch(err) {
                console.log('Fallo al guardar resultado de test');
                throw err;
            }
        });
    }
    catch(err) {
        console.log('Fallo al ejecutar tests');
        console.error(err);
    }
    */
});

app.get('/algorithm-results', async (req, res) => {
    console.log('solicitando resultados de todos los algoritmos');
    try {
        const measureResults = await MeasureResult.find();
        const results = measureResults.reduce((measures: {[key: string]: any[]}, measureResult) => {
            if (!(measureResult.algorithm in measures)) {
                measures[measureResult.algorithm] = [];
            }

            measures[measureResult.algorithm].push(measureResult);

            return measures;
        }, {})

        console.log('results', results);
        res.json({ ok: true, data: results });
    }
    catch (err) {
        res.status(500).json({ ok: false, message: 'Ha ocurrido un error', error: err })
    }
});

app.get('/algorithm-results/:algorithm', async (req, res) => {
    const algorithm = req.params.algorithm;
    console.log('solicitando resultados de algoritmo', algorithm);
    try {
        const results = await MeasureResult.find({ algorithm });
        console.log('results', results);
        res.json({ ok: true, data: results });
    }
    catch (err) {
        res.status(500).json({ ok: false, message: 'Ha ocurrido un error', error: err })
    }
});

app.listen(config.PORT, () => {
    console.log(`Servidor corriendo en puerto ${config.PORT}`);
});