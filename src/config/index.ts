'use strict';
import fs from 'fs';
import path from 'path';
interface ProcessEnv extends NodeJS.ProcessEnv {
    MONGODB_URI: string;
}
declare var process: {
    env: ProcessEnv
}

if (fs.existsSync(path.join(__dirname, 'secrets.json'))) {
    console.log('Obteniendo variables de configuración de secrets.json');
    Object.assign(process.env, require('./secrets.json'));
}
interface IConfig {
    PORT: number;
    MONGODB_URI: string;
}

const config: IConfig = {
    PORT: Number(process.env.PORT) || 8080,
    MONGODB_URI: process.env.MONGODB_URI
}
export default config;