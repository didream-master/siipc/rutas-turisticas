import mongoose, { Schema } from 'mongoose';
import config from '../index';
import { BaseOptions } from '../../algorithms/base.algorithm';

export interface IMeasureResult extends mongoose.Document {
    algorithm: string;
    averages: {
        cost: number;
        timeExecution: number;
    },
    executions: number,
    testParams: BaseOptions & { iterations: number }
}

export const MeasureResult = mongoose.model<IMeasureResult>('MeasureResult', new Schema({
        algorithm: {
            type: String
        },
        averages: {
            cost: {
                type: Number
            },
            timeExecution: {
                type: Number
            }
        },
        executions: Number,
        testParams: Schema.Types.Mixed
    },
    {
        strict: false,
        versionKey: false
    }
));

export default class Database {
    public static async connect() {
        try {
            const db = await mongoose.connect(config.MONGODB_URI, {useNewUrlParser: true});
            console.log('Conexión establecida con la base de datos');
            return db;
        }
        catch(err) {
            console.log('Ha surgido un error al intentar conectarse a la base de datos');
            console.error(err);
            throw err;
        }
    }
}