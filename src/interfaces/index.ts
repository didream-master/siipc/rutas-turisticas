export type NodeId = number;

export interface Neighbor {
    nodeId: NodeId;
    cost: number;
}
export interface Node {
    nombre: string;
    nodeId: NodeId

    costs: {[key: number]: number} // {nodeId => cost}

    // vecinos ordenados en base al tiempo de menor a mayor
    neighbors: Neighbor[];
}
export interface Location extends Node {
    id: number;
    latitud: number;
    longitud: number;
    
    categoria: string;
}