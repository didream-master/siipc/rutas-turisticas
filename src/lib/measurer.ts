import { BaseAlgorithm } from "../algorithms/base.algorithm";
const DEFAULT_TESTS_BY_ITERATION = 10;

export class Measurer<T extends BaseAlgorithm, S> {
    private algorithmInstance: T;
    private algorithmOptions: S[];
    private iterationsByExecution: number[];
    private tests: number;

    constructor(instance: T, options: {algorithmOptions: S[], iterationsByExecution: number[], tests?: number}) {
        this.algorithmInstance = instance;

        const {algorithmOptions, iterationsByExecution, tests = DEFAULT_TESTS_BY_ITERATION} = options;
        this.algorithmOptions = algorithmOptions;
        this.iterationsByExecution = iterationsByExecution;
        this.tests = tests;
    }

    public async measure(resultExecutionHandler?: (resultExecution: any) => any ) {
        for (let iterations of this.iterationsByExecution) {
            for (let algorithmOption of this.algorithmOptions) {
                let sumCosts = 0;
                let sumTimeExecution = 0;
                console.group(`${this.algorithmInstance.constructor.name} running ${iterations} iterations`);
                for (let testNumber = 0; testNumber < this.tests; testNumber++ ) {
                    const start = new Date().getTime();
                    const cost = this.algorithmInstance.run({iterations, ...algorithmOption});
                    const end = new Date().getTime();
                    sumTimeExecution += (end - start);
                    sumCosts += cost;
                }
                resultExecutionHandler && await resultExecutionHandler({
                    algorithm: this.algorithmInstance.constructor.name,
                    testParams: {
                        iterations,
                        ...algorithmOption
                    },
                    averages: {
                        cost: sumCosts / this.tests,
                        timeExecution: sumTimeExecution / this.tests
                    }
                })
                console.log('media coste', sumCosts / this.tests, 'media tiempo', sumTimeExecution / this.tests)
                console.groupEnd();
            }
        }
    }
}