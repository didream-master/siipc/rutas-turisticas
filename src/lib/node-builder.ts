import Utils from "../utils";
import { Node, Neighbor, NodeId } from '../interfaces'; 

type NodeCosts = {[key: string]: string} & {origen: string};

export class NodeBuilder<T extends Node> {
    public nodes: T[];
    public mustFilter: Function;
    constructor ({nodesFile, costsFile, filter}: {nodesFile: string, costsFile: string, filter?: (nodeA: T, nodeB: T) => boolean}){
        this.mustFilter = filter;
        this.initializeNodes(nodesFile);
        this.loadCosts(costsFile);
    }

    public initializeNodes(nodesFile: string) {
        this.nodes = Utils.csvToObjects(nodesFile);
        this.mustFilter && this.filterNodes(this.nodes);
        this.markNodes(this.nodes);
    }

    public filterNodes(nodes: T[]): T[] {
        for(let i=0; i< nodes.length; i++) {
            const node: T = nodes[i];
            let j = i + 1;

            while(j<nodes.length) {
                const l = nodes[j];
        
                
                if (this.mustFilter(node, l)) {
                    nodes.splice(j,1);
                }
                else {
                    j++;
                }
            }
        }
        return nodes;
    }

    public markNodes(nodes: T[]) {
        nodes.forEach((node: T, index) => {
            node.nodeId = index;
        })
    }

    public loadCosts(costsFile: string) {
        const costs: NodeCosts[] = Utils.csvToObjects(costsFile);

        const nodeNamesToId: {[key: string]: NodeId} = this.nodes.reduce((nodeNamesToId: any, node: T): any => {
            nodeNamesToId[Utils.normalizeText(node.nombre)] = node.nodeId;
            return nodeNamesToId;
        }, {});

        costs.forEach((nodeCosts: NodeCosts) => {
            const nodeIndex = nodeNamesToId[Utils.normalizeText(nodeCosts.origen)];
            const node: T = this.nodes[nodeIndex];
        
            delete nodeCosts.origen;
        
            node.costs = {};
            Object.keys(nodeCosts).forEach((nodeName: string) => {
                const neighborIndex: number = nodeNamesToId[Utils.normalizeText(nodeName)];
                
                if (neighborIndex == null || !this.nodes[neighborIndex]) throw new Error(`No se ha podido encontrar ${nodeName} en las localizaciones.`);
                
                node.costs[neighborIndex] = Number(nodeCosts[nodeName]);
            })
        
        });

        this.loadCostsNeighbors();
    }

    public loadCostsNeighbors() {
        this.nodes
            .forEach(node => {
                // se quita el coste 0 (coste hacia la propia localización)
                delete node.costs[node.nodeId];

                // se ordenan los vecidos de menor a mayor dependiendo del coste
                node.neighbors = Object.keys(node.costs)
                                        .sort((neighborA: string, neighborB: string) => node.costs[Number(neighborA)] - node.costs[Number(neighborB)])
                                        .map((neighborIndex): Neighbor => {
                                            return {
                                                nodeId: + neighborIndex,
                                                cost: node.costs[+neighborIndex]
                                            }
                                        });
            });
    }

    
}