import { Location } from './interfaces'; 
import { GRASP, GRASPOptions} from './algorithms/grasp.algorithm';
import { SimulatedAnnealing, SimulatedAnnealingOptions } from './algorithms/simulated-annealing.algorithm';
import { TabuSearchAlgorithm, TabuSearchOptions } from './algorithms/tabu-search.algorithm';
import { VariableNeighborhoodSearch, VariableNeighborhoodSearchOptions } from './algorithms/variable-neighborhood-search.algorithm';
import { NodeBuilder } from './lib/node-builder';
import { Measurer } from './lib/measurer';
import Utils from './utils';

let nodes: Location[] = new NodeBuilder<Location>({
    nodesFile: 'localizaciones.csv',
    costsFile: 'tiempos.csv',

    // se ha detectado nodos con nombres repetidos
    filter: (nodeA, nodeB): boolean => {
        return nodeA.id != nodeB.id && Utils.normalizeText(nodeA.nombre) == Utils.normalizeText(nodeB.nombre)
    }
}).nodes;



export default async function main(callbackExecution?: (resultExecution: any) => any): Promise<void> {

    await new Measurer(new VariableNeighborhoodSearch(nodes), {
        algorithmOptions: [
            {
                rlcLength: 3,
                kMax: 2
            },
            {
                rlcLength: 4,
                kMax: 3
            },
            {
                rlcLength: 5,
                kMax: 4
            },
            {
                rlcLength: 6,
                kMax: 5
            }
        ] as VariableNeighborhoodSearchOptions[],
        iterationsByExecution: [5, 10, 15, 20, 25]
    }).measure(callbackExecution);

    await new Measurer(new TabuSearchAlgorithm(nodes), {
        algorithmOptions: [
            {
                rlcLength: 3
            },
            {
                rlcLength: 4
            },
            {
                rlcLength: 5
            },
            {
                rlcLength: 6
            }
        ] as TabuSearchOptions[],
        iterationsByExecution: [5, 10, 15, 20, 25]
    }).measure(callbackExecution);

    await new Measurer(new SimulatedAnnealing(nodes), {
        algorithmOptions: [
            {
                rlcLength: 3,
                temperature: 10,
                coolingRate: 0.05
            },
            {
                rlcLength: 4,
                temperature: 10,
                coolingRate: 0.1
            },
            {
                rlcLength: 5,
                temperature: 20,
                coolingRate: 0.05
            },
            {
                rlcLength: 6,
                temperature: 20,
                coolingRate: 0.2
            }
        ] as SimulatedAnnealingOptions[],
        iterationsByExecution: [5, 10, 15, 20, 25]
    }).measure(callbackExecution);

    await new Measurer(new GRASP(nodes), {
        algorithmOptions: [
            {
                rlcLength: 3
            },
            {
                rlcLength: 4
            },
            {
                rlcLength: 5
            },
            {
                rlcLength: 6
            }
        ] as GRASPOptions[],
        iterationsByExecution: [5, 10, 15, 20, 25]
    }).measure(callbackExecution);
}