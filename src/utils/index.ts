'use strict';
import fs from 'fs';
import path from 'path';

export default class Utils {
    public static csvToObjects(fileName: string, delimiter = ';;') {
        const filePath = path.join(__dirname, '../data', fileName);
    
        const fileContent = fs.readFileSync(filePath, {encoding: 'utf-8'});
        
        const lines = fileContent.replace(/\r/g, '').split('\n');
    
        const headers = lines.shift().split(delimiter);
    
        const objects = lines.map(line => {
            const values = line.split(delimiter);
            return headers.reduce((obj: any, header, index) => {
                obj[header] = values[index];
                return obj;
            }, {});
            
        });
        return objects;
    }

    public static normalizeText(text: string) {
        return text.normalize('NFD').replace(/[\u0300-\u036f]/g,"").toLocaleLowerCase().trim();
    }
}



